#pragma once

#include "gui/Window.hpp"
#include "gui/CommonControls.hpp"

#include <string>
#include <vector>
#include <queue>


namespace FW {

struct Vertex
{
	Vec3f position;
	Vec3f normal;
};

struct glGeneratedIndices
{
	GLuint static_vao, dynamic_vao;
	GLuint shader_program;
	GLuint static_vertex_buffer, dynamic_vertex_buffer;
	GLuint model_to_world_uniform, world_to_clip_uniform, shading_toggle_uniform, transpose_inverted_uniform;
};

class App : public Window::Listener
{
private:
	enum CurrentModel
	{
		MODEL_EXAMPLE,
		MODEL_USER_GENERATED,
		MODEL_FROM_INDEXED_DATA,
		MODEL_FROM_FILE
	};

public:
						App();		// constructor
	virtual				~App() {}	// destructor

	virtual bool		handleEvent(const Window::Event& ev);

private:
						App(const App&);		// forbid copy
	App&				operator=(const App&);	// forbid assignment

	void				initRendering();
	void				render();
	std::vector<Vertex>	loadObjFileModel(std::string filename);

	Window				window_;
	CommonControls		common_ctrl_;

	CurrentModel		current_model_;
	bool				model_changed_;
	bool				shading_toggle_;
	bool				shading_mode_changed_;

	glGeneratedIndices	gl_;

	std::vector<Vertex>	vertices_;

	float				camera_rotation_angle_;

	// YOUR CODE HERE (R1)
	// Add a class member to store the current translation.
	enum Translation
	{
		MOVE_UP,
		MOVE_DOWN,
		MOVE_RIGHT,
		MOVE_LEFT,
		XSCALE_IN,
		XSCALE_OUT,
		YSCALE_IN,
		YSCALE_OUT,
		ROTATE_LEFT,
		ROTATE_RIGHT,
		NO_MOVE
	};					    // y move, x move,    scale,  rotation
	int	currTranslation[8] = {      0,      0,        0,         0,  0,0,0,0};

	Vec4f currCoord = Vec4f(0,0,0,1);
	double currRot = 0;
	double currXScale = 1;
	// EXTRA:
	// For animation extra credit you can use the framework's Timer class.
	// The .start() and .unstart() methods start and stop the timer; when it's
	// running, .end() gives you seconds passed after last call to .end().
	// Timer timer_;
	boolean animation = false;
	Timer anTimer;
	double anTime;
	double anAngle = 0;
	Vec2i moCamera, moObject;
	boolean moKeyL = false;
	boolean moKeyR = false;
	boolean moMoved = false;
	Mat4f camAng;
	Mat4f objAng;
	Vec4f movCamera = Vec4f(0,0,0,0);
	Vec3f camPos = Vec3f(0, 0, 2.1f);
};

}
