﻿# MC-C3100 Computer Graphics, Fall 2017
# Lehtinen / Kemppinen, Ollikainen, Aarnio
#
# Assignment 1: Introduction

Student name: Jorge Galindo Morata
Student number:
Hours spent on requirements (approx.): Not sure.
Hours spent on extra credit (approx.): Not sure. (I'm filling this with almost everything already done, and I don't remember how much time I used for the different parts, for next assingment I'll be uploading this every once in a while)

# First, some questions about where you come from and how you got started.
# Your answers in this section will be used to improve the course.
# They will not be judged or affect your points, but please answer all of them.
# Keep it short; answering shouldn't take you more than 5-10 minutes.

- What are you studying here at Aalto? (Department, major, minor...?) Exchange Student at Computer Science, at home University Bachelor at Mathematics and Computering at Universidad Politecnica de Madrid. 

- Which year of your studies is this? 4th.

- Is this a mandatory course for you? No.

- Have you had something to do with graphics before? Other studies, personal interests, work? I started a 2D java game using pixels arrays for the tiles and the whole screen, and the different characters.

- Do you remember basic linear algebra? Matrix and vector multiplication, cross product, that sort of thing? Yes.

- How is your overall programming experience? What language are you most comfortable with? I have mostly programmed using C, Java and some mathematical languages like matlab, maple and python focused on it's maths libraries. Also some declarative programming with Haskell and Prolog.

- Do you have some experience with these things? (If not, do you have experience with something similar such as C or Direct3D?)
C++: No, but I have a lot of experience on C and Java, so I shouldn't have a lot of problems with C++.
C++11: No.
OpenGL: No.

- Have you used a version control system such as Git, Mercurial or Subversion? Which ones? Yes, git with GitLab.

- Did you go to the technology lecture? I went to all the lectures (even if to some I got there late).

- Did you go to exercise sessions? Yes, to the last one.

- Did you work on the assignment using Aalto computers, your own computers, or both? Both

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

(Try to get everything done! Based on previous data, virtually everyone who put in the work and did well in the first two assignments ended up finishing the course, and also reported a high level of satisfaction at the end of the course.)

                            opened this file (0p): done
                         R1 Moving an object (1p): done
R2 Generating a simple cone mesh and normals (3p): done
  R3 Converting mesh data for OpenGL viewing (3p): done
           R4 Loading a large mesh from file (3p): done

# Did you do any extra credit work?

I did version control with git (there's a txt with a log of it), rotate with q and e and scaling on the x axis with z and x, ´
the medium difficulty shader transform, an animation with r, mouse control, left clicking lets you change the aiming direction of the camera,
right clicking lets you rotate the world, and with the wheel you can get nearer or farther from it (not really tested as I don't have that
in my laptop, may not work), that function is also added at the i, k buttons, and with j,l you can move the camera along the x axis,
its also added the screen resizing, the mid difficulty one, the easy is also there, but commented, and it lets you also add .ply objects.


(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)

# Are there any known problems/bugs remaining in your code?
The .ply files reading only works with objects with only 3 vertex per face, as in the rest of the program we're using this ones only,
if you try to charge an object with more vertex it only takes the 3 first of them, and doesn't even check the number of vertexes there are.

(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

# Did you collaborate with anyone in the class?
I had some help of the TA in some of the extra credit points in the last exercise session.

(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

# Any other comments you'd like to share about the assignment or the course so far?
When I started with it, some of the things needed for the first exercise (the homogeneous transform matrix) hadn't been already explained
(actually I already knew homogeneous coordinates and affine matrix from some maths courses, but didn't know that that matrix was that), 
so maybe it would be a good idea to say in what lectures are explained the contents needed for the assingment so we know if we have already
gone through it or not yet, also I learnt a lot from this assignment as I had never done any graphics thing, neither programmed in c++,
and, having a good maths base, it wasn't really hard.
(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)

